package migrator;

import com.day1coders.saltandpepper.shaker.engine.ShakerEngine;
import com.ddt.krytpkeypr.Shaker;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author bilorge
 */
public class Migrator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        final String PASSPHRASE = "com.day1coders.saltandpepper.shaker.PASSPHARASE"; //comes from android framework. this is the value used for a key and "pp" holds the value.
        String clave = "uno";
//        String pattern = "Aa*@!$#&&";
        String pattern = "@#*@#*@#*@#*";

        Shaker shaker = new Shaker();
        shaker.setCreateDate(System.currentTimeMillis());
        shaker.setName("1source");
        shaker.setPassPhrase(clave.subSequence(0, clave.toCharArray().length));
        shaker.setPattern(pattern);
        shaker.setUser("bilorges");
        shaker.setVariance("");
        shaker.setVersion(1);
        
        try {
//            System.out.println(clave + shaker.getName().toString() + shaker.getUser().toString() + shaker.getVariance());
            String pwd = ShakerEngine.generatePassword(pattern, clave + shaker.getName().toString() + shaker.getUser().toString() + shaker.getVariance());
            System.out.println(pwd);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Migrator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Migrator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
