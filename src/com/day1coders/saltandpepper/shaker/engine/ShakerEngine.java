package com.day1coders.saltandpepper.shaker.engine;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author bilorge
 */
public class ShakerEngine {
    private static final char[] SYMBOLS = {'"', '`', '~', '.', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '/', '\\', '{', '}', '[', ']', '-', '+', ',', '"', '\''};
    private static final char[] ALPHAS = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    private static final char[] NUMBERS = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
    private static int passTracker = 1;

    /**
     * The pattern values are as follows:
     *
     * A - Uppercase random alpha character - done! 
     * a - Lower case random alpha character - done! 
     * * - Random case alpha character - done! 
     * # - Random numeric character - done 
     * ! - Random alphanumeric character, all alpha characters are upper case 
     * $ - Random alphanumeric character, all alpha characters are lower case 
     * @ - Random mixed case alphanumeric character
     * & - Random symbol
     *
     * @param pattern
     * @param digest
     * @return
     **/
    private static String genPassword(String pattern, byte[] digest) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);
            switch (c) {
                case '#': {//Random numeric character
                    int b = Math.abs(digest[passController(i, digest)]);
                    if((b / NUMBERS.length) != 0) {
                        b = b - (NUMBERS.length * (b / NUMBERS.length));
                    }
                    sb.append(NUMBERS[b]);
                    break;
                }
                case 'a': {//Lower case random alpha character
                    int b = Math.abs(digest[passController(i, digest)]);
                    if((b / ALPHAS.length) != 0) {
                        b = b - (ALPHAS.length * (b / ALPHAS.length));
                    }
                    sb.append(String.valueOf(ALPHAS[b]).toLowerCase());
                    break;
                }
                case 'A': {//Uppercase random alpha character
                    int b = Math.abs(digest[passController(i, digest)]);
                    if((b / ALPHAS.length) != 0) {
                        b = b - (ALPHAS.length * (b / ALPHAS.length));
                    }
                    sb.append(String.valueOf(ALPHAS[b]));
                    break;
                }
                case '*': {//Random case alpha character
                    int b = Math.abs(digest[passController(i, digest)]);
                    int choice = b;
                    if((b / ALPHAS.length) != 0) {
                        b = b - (ALPHAS.length * (b / ALPHAS.length));
                    }

                    if ((choice % 2) == 0) {
                        sb.append(String.valueOf(ALPHAS[b]));
                    } else {
                        sb.append(String.valueOf(ALPHAS[b]).toLowerCase());
                    }
                    break;
                }
                case '!': {//Random alphanumeric character, all alpha characters are upper case 
                    int b = Math.abs(digest[passController(i, digest)]);
                    if (((digest.length + b) % 2) == 0) {
                        if ((b / NUMBERS.length) != 0) {
                            b = b - (NUMBERS.length * (b / NUMBERS.length));
                        }
                        sb.append(NUMBERS[b]);
                    } else {
                        if ((b / ALPHAS.length) != 0) {
                            b = b - (ALPHAS.length * (b / ALPHAS.length));
                        }
                        sb.append(String.valueOf(ALPHAS[b]));
                    }
                    break;
                }
                case '$': {//Random alphanumeric character, all alpha characters are lower case 
                    int b = Math.abs(digest[passController(i, digest)]);
                    if (((digest.length + b) % 2) == 0) {
                        if ((b / NUMBERS.length) != 0) {
                            b = b - (NUMBERS.length * (b / NUMBERS.length));
                        }
                        sb.append(NUMBERS[b]);
                    } else {
                        if ((b / ALPHAS.length) != 0) {
                            b = b - (ALPHAS.length * (b / ALPHAS.length));
                        }
                        sb.append(String.valueOf(ALPHAS[b]).toLowerCase());
                    }
                    break;
                }
                case '@': {//Random mixed case alphanumeric character
                    int b = Math.abs(digest[passController(i, digest)]);
                    int choice = b;
                    switch((digest.length + b + 1) % 3){
                        case 1: {
                            if ((b / NUMBERS.length) != 0) {
                                b = b - (NUMBERS.length * (b / NUMBERS.length));
                            }
                            sb.append(NUMBERS[b]);
                            break;
                        }
                        case 0: {
                            if ((b / ALPHAS.length) != 0) {
                                b = b - (ALPHAS.length * (b / ALPHAS.length));
                            }
                            sb.append(String.valueOf(ALPHAS[b]));
                            break;
                        }
                        case 2: {
                            if ((b / ALPHAS.length) != 0) {
                                b = b - (ALPHAS.length * (b / ALPHAS.length));
                            }
                            sb.append(String.valueOf(ALPHAS[b]).toLowerCase());
                            break;
                        }
                    }
                    break;
                }
                case '&':{
                    int b = Math.abs(digest[passController(i, digest)]);
                    if((b / SYMBOLS.length) != 0) {
                        b = b - (SYMBOLS.length * (b / SYMBOLS.length));
                    }
                    sb.append(String.valueOf(SYMBOLS[b]).toLowerCase());
                    break;
                }
            }
        }

        passTracker = 1;
        return sb.toString();
    }

    private static int passController(int location, byte[] arr){
        if(location < arr.length) {
            return location;
        } else {
            if(location > (arr.length * passTracker)){
                passTracker += 1;
            }
            return (arr.length * passTracker) - location;
        }
    }

    private static byte[] digester(String salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] digest = md5.digest(salt.getBytes("UTF-8"));
        for (int i = 0; i < digest.length; i++) {
            System.out.print(Math.abs(digest[i]) + " ");
        }
        System.out.println("");
        return digest;
    }

    /**
     *
     * A - Uppercase random alpha character - done!
     * a - Lower case random alpha character - done!
     * * - Random case alpha character - done!
     * # - Random numeric character - done
     * ! - Random alphanumeric character, all alpha characters are upper case
     * $ - Random alphanumeric character, all alpha characters are lower case
     * @ - Random mixed case alphanumeric character
     * & - Random symbol
     *
     * @param pattern
     * @param salt
     * @return
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     */
    public static String generatePassword(String pattern, String salt) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if (pattern == null || pattern.equals("") || pattern.length() < 1) {
            return ""; // nothing to do....
        }
        byte[] digest = digester(salt);
        return genPassword(pattern, digest);
    }
}
