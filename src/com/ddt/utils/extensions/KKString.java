/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ddt.utils.extensions;

import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author bilorge
 */
public class KKString {

    private String payload;

    public KKString(String payload) {
        this.payload = payload;
    }

    public KKString(char[] payload) {
        this.payload = Arrays.toString(payload);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.payload);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KKString other = (KKString) obj;
        if (!Objects.equals(this.payload, other.payload)) {
            return false;
        }
        return true;
    }

}
