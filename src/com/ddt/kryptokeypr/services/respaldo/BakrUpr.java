package com.ddt.kryptokeypr.services.respaldo;

import com.ddt.krytpkeypr.SecurityResponse;
import com.ddt.krytpkeypr.Shaker;
import java.util.ArrayList;
import java.util.List;

/**
 * @author bilorge Sep 23, 2016 10:07:14 AM
 */
public class BakrUpr {

    private List<BKElement> elementList;

    public BakrUpr(int numOfElements, List<Shaker> shakers) {
//        elementList = new ArrayList<>(numOfElements);
//        for (Shaker shaker : shakers) {
//            elementList.add(new BKElement(shaker, getResponsesForShaker(shaker.getId())));
//        }
    }

    public BakrUpr(int numOfElements) {
        elementList = new ArrayList<>(numOfElements);
    }

//    private List<SecurityResponse> getResponsesForShaker(String id) {
//        List<SecurityResponse> answers;
//        ResponseDBhelper rdb = new ResponseDBhelper(getBaseContext());
//        SQLiteDatabase dbRef = rdb.getReadableDatabase();
//        String[] select = new String[]{ResponseContract.ResponseEntitiy.FK_SHAKERID, ResponseContract.ResponseEntitiy.RESPONSE, ResponseContract.ResponseEntitiy.PARAMETERS};
//        String where = ResponseContract.ResponseEntitiy.FK_SHAKERID + " = '" + id + "'";
//        Cursor cur = dbRef.query(false, ResponseContract.ResponseEntitiy.TABLE_NAME, select, where, null, null, null, ResponseContract.ResponseEntitiy.RESPONSE + " ASC", null);
//        cur.moveToFirst();
//
//        if (cur.getCount() > 0) {
//            answers = new ArrayList<>(cur.getCount());
//            for (int idx = 0; idx < cur.getCount(); idx++) {
//                answers.add(new SecurityResponse(cur.getString(0), cur.getBlob(1), cur.getBlob(2)));
//                cur.moveToNext();
//            }
//            return answers;
//        }
//        return null;
//    }
    
    public List<BKElement> getElementsForBackUp() {
        return elementList;
    }

    public BKElement addShsker(Shaker shaker) {
        BKElement bkElement = new BKElement(shaker, new ArrayList<>());
        elementList.add(bkElement);
        return bkElement;
    }

    public class BKElement {

        private Shaker shaker;
        private List<SecurityResponse> answers;

        public BKElement(Shaker shaker, List<SecurityResponse> answers) {
            this.shaker = shaker;
            this.answers = answers;
        }

        public Shaker getShaker() {
            return shaker;
        }

        public List<SecurityResponse> getAnswers() {
            if (answers == null) {
                answers = new ArrayList<>();
            }
            return answers;
        }

        public void addAnswer(SecurityResponse response) {
            answers.add(response);
        }
    }
}
