/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ddt.kryptokeypr.services.respaldo;

import com.ddt.krytpkeypr.SecurityResponse;
import com.ddt.krytpkeypr.Shaker;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author bilorge Sep 23, 2016 10:14:55 AM
 */
public class RespaldoService {

    public void exportShakers(BakrUpr bakrUprs, String path) throws IOException {
        File file = new File(path, "Shakers.xyf");
        DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));

        dos.writeInt(bakrUprs.getElementsForBackUp().size()); // how many shaker we have to restore;

        for (BakrUpr.BKElement bkElement : bakrUprs.getElementsForBackUp()) {
            Shaker shaker = bkElement.getShaker();
            dos.writeUTF(shaker.getId());
            dos.writeUTF(shaker.getName());
            dos.writeUTF(shaker.getUser().toString());
            dos.writeUTF(shaker.getPattern().toString());
            dos.writeInt(shaker.getVersion());
            dos.writeLong(shaker.getCreateDate());
            dos.writeUTF(shaker.getVariance());
            dos.writeInt(bkElement.getAnswers().size()); //define cuantas reaspuestas tiene el shaker

            for (SecurityResponse secRes : bkElement.getAnswers()) {
                if (secRes.getSecret() != null && secRes.getSecret().length > 1) {
                    dos.writeInt(secRes.getAlgorithmParams().length);
                    dos.write(secRes.getAlgorithmParams());
                    dos.writeInt(secRes.getSecret().length);
                    dos.write(secRes.getSecret());
                }
            }
        }

        dos.flush();
        dos.close();
    }

    public BakrUpr importShakers(File file) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
        DataInputStream dis = new DataInputStream(bis);
        Shaker shaker;
        int numOfShakers = dis.readInt();
        System.out.println("Found " + numOfShakers + " Shakers to import!"); //@todo: need to remove for prod
        BakrUpr shakers = new BakrUpr(numOfShakers);

        for (int i = 0; i < numOfShakers; i++) {
            shaker = new Shaker();
            shaker.setUid(dis.readUTF());
            shaker.setName(dis.readUTF());
            shaker.setUser(dis.readUTF());
            shaker.setPattern(dis.readUTF());
            shaker.setVersion(dis.readInt());
            shaker.setCreateDate(dis.readLong());
            shaker.setVariance(dis.readUTF());

            System.out.println("Imported Shaker: " + shaker.toDetailedString());

            BakrUpr.BKElement bkElement = shakers.addShsker(shaker);
            int responses = dis.readInt();

            for (int x = 0; x < responses; x++) {
                int bytesToRead = dis.readInt();
                byte[] params = new byte[bytesToRead];
                dis.read(params);
                int sizeOfSecrete = dis.readInt();
                byte[] secrete = new byte[sizeOfSecrete];
                dis.read(secrete);
                bkElement.addAnswer(new SecurityResponse(shaker.getId(), secrete, params));
            }
        }

        return shakers;
    }
}
