/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ddt.kryptokeypr.models;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import xypher.data.controllers.models.Shakers;

/**
 * @author bilorge Feb 11, 2019 2:26:07 PM
 */
public class ShakersListCellRenderer extends JLabel implements ListCellRenderer<Shakers> {

    public ShakersListCellRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Shakers> list, Shakers value, int index, boolean isSelected, boolean cellHasFocus) {
        Shakers elem = list.getModel().getElementAt(index);
        String val = "  " + elem.getShaker();
        setText(val);
        
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

//        return new JLabel(val);
        return this;
    }

}
