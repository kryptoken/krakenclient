/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ddt.kryptokeypr.models;

import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import xypher.data.controllers.models.Variants;

/**
 *
 * @author bilorge
 */
public class VariantListModel extends DefaultListModel <Variants>{
    
    private List<Variants> variants;

    public VariantListModel() {
        this.variants = new ArrayList<>();
    }
    
    public VariantListModel(List<Variants> variants) {
        this.variants = variants;
    }
    
    @Override
    public int getSize() {
        if (variants != null && !variants.isEmpty()) {
            return variants.size();
        }
        
        return 0;
    }

    @Override
    public Variants getElementAt(int index) {
        if (index >= 0 && index < variants.size()) {
            return variants.get(index);
        }
        
        return null;
    }

    public void addAll(List<Variants> history) {
        if (history != null && history.size() > 0) {
            variants.addAll(history);
            fireContentsChanged(history, 0, variants.size());
        }
    }
    
}
