/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ddt.kryptokeypr.models;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import xypher.data.controllers.models.Variants;

/**
 *
 * @author bilorge
 */
public class VariantsCellRenderer implements ListCellRenderer<Variants> {

    @Override
    public Component getListCellRendererComponent(JList<? extends Variants> list, Variants value, int index, boolean isSelected, boolean cellHasFocus) {
        Variants element = list.getModel().getElementAt(index);
        String val = " " + element.getValue();

        return new JLabel(val);
    }

}
