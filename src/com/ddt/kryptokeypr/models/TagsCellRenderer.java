/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ddt.kryptokeypr.models;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import xypher.data.controllers.models.Tags;

/**
 *
 * @author bilorge
 */
public class TagsCellRenderer extends JLabel implements ListCellRenderer<Tags>  {

    @Override
    public Component getListCellRendererComponent(JList<? extends Tags> list, Tags value, int index, boolean isSelected, boolean cellHasFocus) {
//        return new JLabel(value.getTags());
        setText(value.getTags());
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        return this;
    }
}
