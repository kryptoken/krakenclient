/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ddt.kryptokeypr.models;

import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import xypher.data.controllers.models.Tags;

/**
 *
 * @author bilorge
 */
public class TagListModel extends DefaultListModel<Tags> {

    private List<Tags> tags;

    public TagListModel() {
        tags = new ArrayList<>();
    }

    public TagListModel(List<Tags> tags) {
        this.tags = tags;
    }

    @Override
    public int getSize() {
        return tags.size();
    }

    @Override
    public Tags getElementAt(int index) {
        return tags.get(index);
    }

    public boolean addTag(Tags tag) {
        tags.add(tag);
        fireIntervalAdded(tag, tags.size(), tags.size());
        return true;
    }

    public boolean removeTag(Tags tag) {
        tags.remove(tag);
        fireIntervalRemoved(tags, 0, tags.size());
        return true;
    }
}
