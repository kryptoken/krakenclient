/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ddt.kryptokeypr.models;

import code.factory.CryptoEngineFactory;
import code.portable.ICryptoEngine;
import com.ddt.krytpkeypr.SecurityResponse;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.DefaultListModel;

/**
 *
 * @author bilorge
 */
public class VaultListModel extends DefaultListModel<String> {

    private final List<SecurityResponse> secrets;
    private final ICryptoEngine engineInstance;

    public VaultListModel(List<SecurityResponse> secrets, String masterPassword, char[] password) {
        this.secrets = secrets;
        this.engineInstance = CryptoEngineFactory.getEngineInstance(masterPassword, password);
    }

    @Override
    public int getSize() {
        return secrets.size();
    }

    @Override
    public String getElementAt(int index) {
        SecurityResponse a = secrets.get(index);
        engineInstance.setEncryptionParameters(a.getAlgorithmParams());
        try {
            return engineInstance.dec(a.getSecret());
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException | InvalidParameterSpecException ex) {
            Logger.getLogger(VaultListModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public SecurityResponse getSecRespAtIndex(int index) {
        SecurityResponse element = secrets.get(index);
        return element;
    }

    public void addElementToList(String id, String fk, byte[] secret, byte[] params) {
        SecurityResponse secRes = new SecurityResponse(id, fk, secret, params);
        secrets.add(secRes);
        fireIntervalAdded(secRes, secrets.size(), secrets.size());
    }

    public void deleteElement(int idx) {
        SecurityResponse sr = secrets.get(idx);
        secrets.remove(idx);
        fireIntervalRemoved(sr, idx, idx);
    }

    public SecurityResponse getSelectedObject(int idx) {
        return secrets.get(idx);
    }
}
