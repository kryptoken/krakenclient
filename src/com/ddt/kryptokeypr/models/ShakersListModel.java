/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ddt.kryptokeypr.models;

import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import xypher.data.controllers.models.Shakers;

/**
 * @author bilorge Feb 11, 2019 1:26:56 PM
 */
public class ShakersListModel extends DefaultListModel<Shakers> {

    List<Shakers> payload;

    public ShakersListModel() {
        this.payload = new ArrayList<>();
    }

    public ShakersListModel(List<Shakers> payload) {
        this.payload = payload;
    }

    @Override
    public int getSize() {
        if (payload != null && !payload.isEmpty()) {
            return payload.size();
        }
        return -1;
    }

    @Override
    public Shakers getElementAt(int index) {
        if (index < 0) {
            return null;
        }
        if (index < payload.size()) {
            return payload.get(index);
        }
        return null;
    }
    
    public boolean updateElementAt(int index, Shakers shaker) {
        payload.remove(index);
        payload.add(index, shaker);
        return true;
    }
}
