package com.ddt.xypher.utils;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

/**
 *
 * @author bilorge
 */
public class ClipboardUtils {

    public ClipboardUtils() {
    }

    public static void putInClipboard(String data) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable transferable = new StringSelection(data);
        clipboard.setContents(transferable, null);
    }
    
    public static void putInClipboard(String data, ClipboardOwner owner) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable transferable = new StringSelection(data);
        clipboard.setContents(transferable, owner);
    }
}
