package com.ddt.krytpkeypr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import xypher.data.controllers.models.Tags;

/**
 * @author bilorge
 */
public class Shaker implements Serializable, Comparable {

    private static final long serialVersionUID = 7880634508081337984L;
    private List<SecurityResponse> secQA;
    private String name;
    private CharSequence passPhrase;
    private CharSequence pattern;
    private CharSequence user;
    private long createDate;
    private String uid;
    private int version;
    private String variance;
    private String bucket;
    private List<Tags> tag;

    public Shaker() {
        secQA = new ArrayList<>();
        tag = new ArrayList<>();
        createDate = System.currentTimeMillis();
    }

    public Shaker(String ID, String name, long time, String variant) {
        this.name = name;
        secQA = new ArrayList<>();
        createDate = time;
        uid = ID;
        if (variant == null) {
            variance = "";
        } else {
            variance = variant;
        }
    }

    public String getVariance() {
        return variance;
    }

    public void setVariance(String variance) {
        this.variance = variance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CharSequence getPassPhrase() {
        return passPhrase;
    }

    public void setPassPhrase(CharSequence passPhrase) {
        this.passPhrase = passPhrase;
    }

    public CharSequence getPattern() {
        return pattern;
    }

    public void setPattern(CharSequence pattern) {
        this.pattern = pattern;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public CharSequence getUser() {
        return user;
    }

    public void setUser(CharSequence user) {
        this.user = user;
    }

    public List<SecurityResponse> getSecQA() {
        return secQA;
    }
    
    public void setSecQA(List<SecurityResponse> secQA) {
        this.secQA = secQA;
    }

    public String getId() {
        return uid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int ver) {
        version = ver;
    }

    public long getShakerAge() {
        Long now = System.currentTimeMillis() - createDate;
        return now / 86400000;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long time) {
        createDate = time;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public String toDetailedString() {
        String ls = System.lineSeparator();
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(this.name).append(ls);
        sb.append("Pattern: ").append(this.pattern).append(ls);
        sb.append("ID: ").append(this.uid).append(ls);
        sb.append("Variant: ").append(this.variance).append(ls);
        sb.append("Version: ").append(this.version).append(ls);
        return sb.toString();
    }

    @Override
    public int compareTo(Object another) {
        int i = this.name.toUpperCase().compareTo(((Shaker) another).getName().toUpperCase());
        return i;
    }

    /**
     * @return the tag
     */
    public List<Tags> getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(List<Tags> tag) {
        this.tag = tag;
    }
}
