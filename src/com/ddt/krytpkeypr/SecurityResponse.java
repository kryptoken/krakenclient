package com.ddt.krytpkeypr;

import java.util.UUID;

/**
 * @author bilorge
 */
public class SecurityResponse {

    private final String fkRef;
    private String answer;
    private String identity;
    private final byte[] algorithmParams;
    private byte[] secret;

    public SecurityResponse(String fkRef, String answer, byte[] algorithmParams, String identity) {
        this.fkRef = fkRef;
        this.answer = answer;
        this.algorithmParams = algorithmParams;
        this.identity = identity;
    }

    public SecurityResponse(String ID, String fkRef, byte[] secret, byte[] algorithmParams) {
        this.fkRef = fkRef;
        this.secret = secret;
        this.algorithmParams = algorithmParams;
        this.identity = ID;
    }

    public SecurityResponse(String fkRef, byte[] secret, byte[] algorithmParams) {
        this.fkRef = fkRef;
        this.secret = secret;
        this.algorithmParams = algorithmParams;
        this.identity = UUID.randomUUID().toString();
    }

    public String getFkRef() {
        return fkRef;
    }

    public String getAnswer() {
        return answer;
    }

    public byte[] getSecret() {
        return secret;
    }

    public byte[] getAlgorithmParams() {
        return algorithmParams;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    @Override
    public String toString() {
        return answer;
    }
}
