package code.factory;

import code.factory.impl.security.CryptoEngineImpl;
import code.portable.ICryptoEngine;
import java.security.NoSuchAlgorithmException;
import javax.crypto.NoSuchPaddingException;


/**
 * @author bilorge
 */
public class CryptoEngineFactory {

    public static ICryptoEngine getEngineInstance(String masterPwd, char[] password) {
        try {
            return new CryptoEngineImpl(masterPwd, password);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException e) {
//            e.printStackTrace();
            return null;
        }
    }
}
