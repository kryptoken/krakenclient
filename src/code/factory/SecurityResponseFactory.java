/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.factory;

import com.ddt.krytpkeypr.SecurityResponse;
import java.util.ArrayList;
import java.util.List;
import xypher.data.controllers.models.Responses;

/**
 *
 * @author bilorge
 */
public class SecurityResponseFactory {
    public static List<SecurityResponse> getSecurityResponseList(List<Responses> secQA) {
       List<SecurityResponse> secrets = new ArrayList<>();
       secQA.forEach(resp -> {
           SecurityResponse  secResp = new SecurityResponse(resp.getIdentity(), resp.getFkShakerid().getIdentity(), resp.getResponse(), resp.getParameters());
           secrets.add(secResp);
        });
       
       return secrets;
    }
}
