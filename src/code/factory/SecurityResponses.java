//package code.factory;
//
//import code.portable.ICryptoEngine;
//import com.ddt.krytpkeypr.SecurityResponse;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// *
// * @author bilorge
// */
//public class SecurityResponses {
//    private List<SecurityResponse> respuestas;
////    private Context context;
//    private String key;
//    private CharSequence salt;
//    private String password;
//    private ICryptoEngine cryptoEngine;
//
////    public SecurityResponses(Context context, String shakerID, CharSequence salt, String password, ICryptoEngine cryptoEngine) {
//    public SecurityResponses(String shakerID, CharSequence salt, String password, ICryptoEngine cryptoEngine) {
////        this.context = context;
//        respuestas = new ArrayList<>(5);
//        key = shakerID;
//        this.salt = salt;
//        this.password = password;
//        this.cryptoEngine = cryptoEngine;
//        refreshList();
//    }
//
//    public void refreshList() {
////        ResponseDBhelper rdb = new ResponseDBhelper(context);
////        SQLiteDatabase dbRef = rdb.getReadableDatabase();
////        String[] select = new String[]{ResponseContract.ResponseEntitiy.FK_SHAKERID, ResponseContract.ResponseEntitiy.RESPONSE, ResponseContract.ResponseEntitiy.PARAMETERS, ResponseContract.ResponseEntitiy.IDENTITY};
////        String where = ResponseContract.ResponseEntitiy.FK_SHAKERID + " = '" + key + "'";
////
////        Cursor cur = dbRef.query(false, ResponseContract.ResponseEntitiy.TABLE_NAME, select, where, null, null, null, ResponseContract.ResponseEntitiy.RESPONSE + " ASC", null);
//
////        cur.moveToFirst();
//        respuestas.clear();
//
//        for (int i = 0; i < cur.getCount(); i++) {
//            String answer = "";
//            try {
//                byte[] rsp = cur.getBlob(1);
//                cryptoEngine.setEncryptionParameters(cur.getBlob(2));
//                answer = cryptoEngine.dec(rsp);
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//            } catch (NoSuchPaddingException e) {
//                e.printStackTrace();
//            } catch (InvalidKeyException e) {
//                e.printStackTrace();
//            } catch (InvalidAlgorithmParameterException e) {
//                e.printStackTrace();
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            } catch (IllegalBlockSizeException e) {
//                e.printStackTrace();
//            } catch (BadPaddingException e) {
//                e.printStackTrace();
//            } catch (InvalidKeySpecException e) {
//                e.printStackTrace();
//            } catch (InvalidParameterSpecException e) {
//                e.printStackTrace();
//            } finally {
//                if (answer == null || answer.equals("")) {
//                    answer = "*******************";
////                    try {
////                        answer = new String(cur.getBlob(1), "UTF-8");
////                    } catch (UnsupportedEncodingException e) {
////                        e.printStackTrace();
////                        answer = "<!XXXXX!>";
////                }
//                }
//            }
//            SecurityResponse secRes = new SecurityResponse(cur.getString(0), answer, cur.getBlob(2), cur.getString(3));
//            respuestas.add(secRes);
//            cur.moveToNext();
//        }
//
//        dbRef.close();
//    }
//
//    public void addResponse(SecurityResponse response) {
//        ResponseDBhelper resDB = new ResponseDBhelper(context);
//        SQLiteDatabase wrdb = resDB.getWritableDatabase();
//        ContentValues values = new ContentValues();
//
//        values.put(ResponseContract.ResponseEntitiy.FK_SHAKERID, response.getFkRef());
//        values.put(ResponseContract.ResponseEntitiy.RESPONSE, response.getSecret());
//        values.put(ResponseContract.ResponseEntitiy.PARAMETERS, response.getAlgorithmParams());
//        values.put(ResponseContract.ResponseEntitiy.IDENTITY, UUID.randomUUID().toString());
//
//        wrdb.insert(ResponseContract.ResponseEntitiy.TABLE_NAME, null, values);
//
//        wrdb.close();
//        refreshList();
//    }
//
//    public void removeResponse(int loc) {
//        ResponseDBhelper resDB = new ResponseDBhelper(context);
//        SQLiteDatabase wrdb = resDB.getWritableDatabase();
//
//        SecurityResponse sr = respuestas.get(loc);
//
//        wrdb.delete(ResponseContract.ResponseEntitiy.TABLE_NAME, ResponseContract.ResponseEntitiy.FK_SHAKERID + " = '" + key + "' and " + ResponseContract.ResponseEntitiy.IDENTITY + " = '" + sr.getIdentity() + "'", null);
//        refreshList();
//    }
//
//    public List<SecurityResponse> getResponses() {
//        return respuestas;
//    }
//}
