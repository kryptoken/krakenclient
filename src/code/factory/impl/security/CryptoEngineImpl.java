package code.factory.impl.security;

import code.portable.ICryptoEngine;
import code.portable.Repeater;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author bilorge
 */
public class CryptoEngineImpl implements ICryptoEngine {


    private final List<Repeater> subscribers = new ArrayList<>();

    private SecretKey secretKey = null;
    private Cipher cipher;
    //
    private final int ITERATIONS = 1000;
    private final int KEYLENGTH = 256;
    private final int SALTLENGTH = KEYLENGTH / 8; // same size as key output/

    private char[] password;

    private SecureRandom random = new SecureRandom();

    private final String ALGORITHM = "PBKDF2WithHmacSHA1";
    private final String CIPHERINSTANCE = "AES/CBC/PKCS5Padding";

    private final String masterPassword;

    private final byte[] salt;
    private byte[] iv;

    public CryptoEngineImpl(String masterPassword, char[] pwd) throws NoSuchPaddingException, NoSuchAlgorithmException {
        this.masterPassword = masterPassword;
        this.password = pwd;
        cipher = Cipher.getInstance(CIPHERINSTANCE);

        iv = new byte[cipher.getBlockSize()];
        random.nextBytes(iv); //@todo: needs refactoring

        salt = getSalt();

        try {
            engineInit();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            System.out.println("An Exception took place when initializing the crypto engine.");
        }
    }

    public CryptoEngineImpl(String masterPassword, char[] pwd, byte[] iv) throws NoSuchAlgorithmException, NoSuchPaddingException {
        this.masterPassword = masterPassword;
        this.password = pwd;
        this.iv = iv.clone();

        cipher = Cipher.getInstance(CIPHERINSTANCE);

        salt = getSalt();

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    engineInit();
                } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
                    System.out.println("An Exception took place when initializing the crypto engine.");
                }
            }
        }).start();
    }

    private void engineInit() throws NoSuchAlgorithmException, InvalidKeySpecException {
        /* Derive the key, given password and salt. */
        communicate("Starting Encryption Engine initialization");
        KeySpec keySpec = new PBEKeySpec(password, salt, ITERATIONS, KEYLENGTH);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
        byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();

        //if secret key is not null, then dont wast the cpu cycles
        secretKey = new SecretKeySpec(keyBytes, "AES");
        if (secretKey == null) {
            communicate("Unable to generate secrete key! No bueno"); //@todo: do not forget to remove the wise cracks
        }
        communicate("Encryption engine initialization complete");
    }

    @Override
    public byte[] enc(String secrete) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {

        cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
        byte[] ciphertext = cipher.doFinal(secrete.getBytes("UTF-8"));

        System.out.println(new String(ciphertext, "UTF-8"));

        return ciphertext;
    }

    @Override
    public String dec(byte[] secrete) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, InvalidParameterSpecException {
        /* Decrypt the message, given derived key and initialization vector. */
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
        String plaintext = new String(cipher.doFinal(secrete), "UTF-8");
        return plaintext;
    }

    @Override
    public byte[] getEncryptionParameters() {
        return iv;
    }

    @Override
    public void setEncryptionParameters(byte[] params) {
        iv = params;
    }

    public void subscribe(Repeater r) {
        subscribers.add(r);
    }

    public void unSubscribe(Repeater r) {
        subscribers.remove(r);
    }

    private void communicate(CharSequence msg) {
        for (Repeater r : subscribers) {
            r.psa(msg);
        }
    }

    //this function will return the required 0 padded salt needed.
    private byte[] getSalt() {
        byte[] grains = new byte[SALTLENGTH];
        byte[] bytes = masterPassword.getBytes();
        for (int i = 0; i < grains.length; i++) {
            try {
                grains[i] = bytes[i];
            } catch (IndexOutOfBoundsException ioobx) {
                grains[i] = (byte) (i * 11 * masterPassword.length());
            }
        }
        return grains;
    }
}
