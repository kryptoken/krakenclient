/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher.data.controllers;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import xypher.data.controllers.exceptions.IllegalOrphanException;
import xypher.data.controllers.exceptions.NonexistentEntityException;
import xypher.data.controllers.exceptions.PreexistingEntityException;
import xypher.data.controllers.models.Responses;
import xypher.data.controllers.models.Shakers;
import xypher.data.controllers.models.Tags;
import xypher.data.controllers.models.Users;
import xypher.data.controllers.models.Variants;

/**
 *
 * @author bilorge
 */
public class ShakersJpaController implements Serializable {

    public ShakersJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Shakers shakers) throws PreexistingEntityException, Exception {
        if (shakers.getResponsesSet() == null) {
            shakers.setResponsesSet(new HashSet<Responses>());
        }
        if (shakers.getVariantsSet() == null) {
            shakers.setVariantsSet(new HashSet<Variants>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users fkUserid = shakers.getFkUserid();
            if (fkUserid != null) {
                fkUserid = em.getReference(fkUserid.getClass(), fkUserid.getIduser());
                shakers.setFkUserid(fkUserid);
            }
//            Tags tags = shakers.getTags();
//            if (tags != null) {
//                tags = em.getReference(tags.getClass(), tags.getTagid());
//                shakers.setTags(tags);
//            }
            Set<Responses> attachedResponsesSet = new HashSet<Responses>();
            for (Responses responsesSetResponsesToAttach : shakers.getResponsesSet()) {
                responsesSetResponsesToAttach = em.getReference(responsesSetResponsesToAttach.getClass(), responsesSetResponsesToAttach.getIdentity());
                attachedResponsesSet.add(responsesSetResponsesToAttach);
            }
            shakers.setResponsesSet(attachedResponsesSet);
            Set<Variants> attachedVariantsSet = new HashSet<Variants>();
            for (Variants variantsSetVariantsToAttach : shakers.getVariantsSet()) {
                variantsSetVariantsToAttach = em.getReference(variantsSetVariantsToAttach.getClass(), variantsSetVariantsToAttach.getVariantId());
                attachedVariantsSet.add(variantsSetVariantsToAttach);
            }
            shakers.setVariantsSet(attachedVariantsSet);
            em.persist(shakers);
            if (fkUserid != null) {
                fkUserid.getShakersSet().add(shakers);
                fkUserid = em.merge(fkUserid);
            }
//            if (tags != null) {
//                Shakers oldFkShakeridOfTags = tags.getFkShakerid();
//                if (oldFkShakeridOfTags != null) {
//                    oldFkShakeridOfTags.setTags(null);
//                    oldFkShakeridOfTags = em.merge(oldFkShakeridOfTags);
//                }
//                tags.setFkShakerid(shakers);
//                tags = em.merge(tags);
//            }
            for (Responses responsesSetResponses : shakers.getResponsesSet()) {
                Shakers oldFkShakeridOfResponsesSetResponses = responsesSetResponses.getFkShakerid();
                responsesSetResponses.setFkShakerid(shakers);
                responsesSetResponses = em.merge(responsesSetResponses);
                if (oldFkShakeridOfResponsesSetResponses != null) {
                    oldFkShakeridOfResponsesSetResponses.getResponsesSet().remove(responsesSetResponses);
                    oldFkShakeridOfResponsesSetResponses = em.merge(oldFkShakeridOfResponsesSetResponses);
                }
            }
            for (Variants variantsSetVariants : shakers.getVariantsSet()) {
                Shakers oldFkShakeridOfVariantsSetVariants = variantsSetVariants.getFkShakerid();
                variantsSetVariants.setFkShakerid(shakers);
                variantsSetVariants = em.merge(variantsSetVariants);
                if (oldFkShakeridOfVariantsSetVariants != null) {
                    oldFkShakeridOfVariantsSetVariants.getVariantsSet().remove(variantsSetVariants);
                    oldFkShakeridOfVariantsSetVariants = em.merge(oldFkShakeridOfVariantsSetVariants);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findShakers(shakers.getIdentity()) != null) {
                throw new PreexistingEntityException("Shakers " + shakers + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Shakers shakers) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Shakers persistentShakers = em.find(Shakers.class, shakers.getIdentity());
            persistentShakers.setUsername(shakers.getUsername());
            persistentShakers.setPattern(shakers.getPattern());
            persistentShakers.setShaker(shakers.getShaker());
            em.merge(persistentShakers);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = shakers.getIdentity();
                if (findShakers(id) == null) {
                    throw new NonexistentEntityException("The shakers with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Shakers shakers;
            try {
                shakers = em.getReference(Shakers.class, id);
                shakers.getIdentity();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The shakers with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
//            Tags tagsOrphanCheck = shakers.getTags();
//            if (tagsOrphanCheck != null) {
//                if (illegalOrphanMessages == null) {
//                    illegalOrphanMessages = new ArrayList<String>();
//                }
//                illegalOrphanMessages.add("This Shakers (" + shakers + ") cannot be destroyed since the Tags " + tagsOrphanCheck + " in its tags field has a non-nullable fkShakerid field.");
//            }
            Set<Responses> responsesSetOrphanCheck = shakers.getResponsesSet();
            for (Responses responsesSetOrphanCheckResponses : responsesSetOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Shakers (" + shakers + ") cannot be destroyed since the Responses " + responsesSetOrphanCheckResponses + " in its responsesSet field has a non-nullable fkShakerid field.");
            }
            Set<Variants> variantsSetOrphanCheck = shakers.getVariantsSet();
            for (Variants variantsSetOrphanCheckVariants : variantsSetOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Shakers (" + shakers + ") cannot be destroyed since the Variants " + variantsSetOrphanCheckVariants + " in its variantsSet field has a non-nullable fkShakerid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Users fkUserid = shakers.getFkUserid();
            if (fkUserid != null) {
                fkUserid.getShakersSet().remove(shakers);
                fkUserid = em.merge(fkUserid);
            }
            em.remove(shakers);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Shakers> findShakersEntities() {
        return findShakersEntities(true, -1, -1);
    }

    public List<Shakers> findShakersEntities(int maxResults, int firstResult) {
        return findShakersEntities(false, maxResults, firstResult);
    }

    private List<Shakers> findShakersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Shakers.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Shakers findShakers(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Shakers.class, id);
        } finally {
            em.close();
        }
    }

    public int getShakersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Shakers> rt = cq.from(Shakers.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Shakers> findShakersForUser(Users user) {
        EntityManager em = getEntityManager();
        try {
            Query qry = em.createNamedQuery("Shakers.findFKUserKey");
            qry.setParameter("fkey", user);
            List<Shakers> resultSet = qry.getResultList();
//            Collections.sort(resultSet, new Shakers());
//            Collections.sort(resultSet);
            return resultSet;
        } finally {
            em.close();
        }
    }

}
