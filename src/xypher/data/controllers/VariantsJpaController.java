/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher.data.controllers;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import xypher.data.controllers.exceptions.NonexistentEntityException;
import xypher.data.controllers.exceptions.PreexistingEntityException;
import xypher.data.controllers.models.Shakers;
import xypher.data.controllers.models.Variants;

/**
 *
 * @author bilorge
 */
public class VariantsJpaController implements Serializable {

    public VariantsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Variants variants) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            //edit the active variatn
            Shakers fkShakerid = variants.getFkShakerid();
            Variants active = findActiveVariant(fkShakerid);
            if (active != null) {
                active.setStatus(0);
                edit(active);
            }
            em.getTransaction().begin();
            if (fkShakerid != null) {
                variants.setCreatedate(new Date());
                variants.setStatus(1);
                variants.setVariantId(UUID.randomUUID().toString());
                fkShakerid.getVariantsSet().add(variants);
                fkShakerid = em.merge(fkShakerid);
                em.persist(variants);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findVariants(variants.getVariantId()) != null) {
                throw new PreexistingEntityException("Variants " + variants + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Variants variants) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Variants reference = em.getReference(Variants.class, variants.getVariantId());
            reference.setStatus(variants.getStatus());
            em.merge(reference);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = variants.getVariantId();
                if (findVariants(id) == null) {
                    throw new NonexistentEntityException("The variants with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Variants variants;
            try {
                variants = em.getReference(Variants.class, id);
                variants.getVariantId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The variants with id " + id + " no longer exists.", enfe);
            }
            Shakers fkShakerid = variants.getFkShakerid();
            if (fkShakerid != null) {
                fkShakerid.getVariantsSet().remove(variants);
                fkShakerid = em.merge(fkShakerid);
            }
            em.remove(variants);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Variants> findVariantsEntities() {
        return findVariantsEntities(true, -1, -1);
    }

    public List<Variants> findVariantsEntities(int maxResults, int firstResult) {
        return findVariantsEntities(false, maxResults, firstResult);
    }

    private List<Variants> findVariantsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Variants.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Variants findVariants(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Variants.class, id);
        } finally {
            em.close();
        }
    }

    public List<Variants> findVariantsByShakerID(Shakers shakerID) {
        EntityManager em = getEntityManager();
        try {
            Query qry = em.createNamedQuery("Variants.findByShakerID");
            qry.setParameter("shakerid", shakerID);
            List<Variants> resultList = qry.getResultList();
            return resultList;
        } finally {
            em.close();
        }
    }

    public int getVariantsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Variants> rt = cq.from(Variants.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public Variants findActiveVariant(Shakers id) {
        EntityManager em = getEntityManager();
        try {
            Query qry = em.createNamedQuery("Variants.findActiveVariantsForShaker");
            qry.setParameter("shakerid", id);
            return (Variants) qry.getSingleResult();
        } catch (NoResultException nr) {
            return null;
        } finally {
            em.close();
        }
    }

    public List<Variants> findActiveVariant(String shakerID) {
        EntityManager em = getEntityManager();
        try {
            Query qry = em.createNamedQuery("Variants.findByShakerID");
            qry.setParameter("shakerid", shakerID);
            return qry.getResultList();
        } catch (NoResultException nr) {
            return null;
        } finally {
            em.close();
        }
    }
}
