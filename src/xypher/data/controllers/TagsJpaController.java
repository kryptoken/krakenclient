/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher.data.controllers;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import xypher.data.controllers.exceptions.IllegalOrphanException;
import xypher.data.controllers.exceptions.NonexistentEntityException;
import xypher.data.controllers.exceptions.PreexistingEntityException;
import xypher.data.controllers.models.Shakers;
import xypher.data.controllers.models.Tags;

/**
 *
 * @author bilorge
 */
public class TagsJpaController implements Serializable {

    public TagsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Tags tags) throws IllegalOrphanException, PreexistingEntityException, Exception {
        if (tags.getFkShakerid() == null) {
            throw new Exception("Foreign Key can not be null");
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(tags);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTags(tags.getTagid()) != null) {
                throw new PreexistingEntityException("Tags " + tags + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Tags tags) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tags persistentTags = em.find(Tags.class, tags.getTagid());
            Shakers fkShakeridOld = persistentTags.getFkShakerid();
            Shakers fkShakeridNew = tags.getFkShakerid();
            List<String> illegalOrphanMessages = null;
//            if (fkShakeridNew != null && !fkShakeridNew.equals(fkShakeridOld)) {
//                Tags oldTagsOfFkShakerid = fkShakeridNew.getTags();
//                if (oldTagsOfFkShakerid != null) {
//                    if (illegalOrphanMessages == null) {
//                        illegalOrphanMessages = new ArrayList<String>();
//                    }
//                    illegalOrphanMessages.add("The Shakers " + fkShakeridNew + " already has an item of type Tags whose fkShakerid column cannot be null. Please make another selection for the fkShakerid field.");
//                }
//            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (fkShakeridNew != null) {
                fkShakeridNew = em.getReference(fkShakeridNew.getClass(), fkShakeridNew.getIdentity());
                tags.setFkShakerid(fkShakeridNew);
            }
            tags = em.merge(tags);
            if (fkShakeridOld != null && !fkShakeridOld.equals(fkShakeridNew)) {
                fkShakeridOld.setTags(null);
                fkShakeridOld = em.merge(fkShakeridOld);
            }
//            if (fkShakeridNew != null && !fkShakeridNew.equals(fkShakeridOld)) {
//                fkShakeridNew.setTags(tags);
//                fkShakeridNew = em.merge(fkShakeridNew);
//            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = tags.getTagid();
                if (findTags(id) == null) {
                    throw new NonexistentEntityException("The tags with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tags tags;
            try {
                tags = em.getReference(Tags.class, id);
                tags.getTagid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tags with id " + id + " no longer exists.", enfe);
            }
            Shakers fkShakerid = tags.getFkShakerid();
            if (fkShakerid != null) {
                fkShakerid.setTags(null);
                fkShakerid = em.merge(fkShakerid);
            }
            em.remove(tags);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Tags> findTagsEntities() {
        return findTagsEntities(true, -1, -1);
    }

    public List<Tags> findTagsEntities(int maxResults, int firstResult) {
        return findTagsEntities(false, maxResults, firstResult);
    }

    private List<Tags> findTagsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Tags.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Tags findTags(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Tags.class, id);
        } finally {
            em.close();
        }
    }
    
    public List<Tags> findTagsByShakerID(Shakers shaker) {
        EntityManager em = getEntityManager();
        try {
            Query qry = em.createNamedQuery("Tags.findByForeignKey");
            qry.setParameter("shakerid", shaker);
            return qry.getResultList();
        } finally {
            em.close();
        }
    }

    public int getTagsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Tags> rt = cq.from(Tags.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
