/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher.data.controllers;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import xypher.data.controllers.exceptions.NonexistentEntityException;
import xypher.data.controllers.exceptions.PreexistingEntityException;
import xypher.data.controllers.models.Responses;
import xypher.data.controllers.models.Shakers;

/**
 *
 * @author bilorge
 */
public class ResponsesJpaController implements Serializable {

    public ResponsesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Responses responses) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Shakers fkShakerid = responses.getFkShakerid();
            if (fkShakerid != null) {
                fkShakerid = em.getReference(fkShakerid.getClass(), fkShakerid.getIdentity());
                responses.setFkShakerid(fkShakerid);
            }
            em.persist(responses);
            if (fkShakerid != null) {
                fkShakerid.getResponsesSet().add(responses);
                fkShakerid = em.merge(fkShakerid);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findResponses(responses.getIdentity()) != null) {
                throw new PreexistingEntityException("Responses " + responses + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Responses responses) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Responses persistentResponses = em.find(Responses.class, responses.getIdentity());
            Shakers fkShakeridOld = persistentResponses.getFkShakerid();
            Shakers fkShakeridNew = responses.getFkShakerid();
            if (fkShakeridNew != null) {
                fkShakeridNew = em.getReference(fkShakeridNew.getClass(), fkShakeridNew.getIdentity());
                responses.setFkShakerid(fkShakeridNew);
            }
            responses = em.merge(responses);
            if (fkShakeridOld != null && !fkShakeridOld.equals(fkShakeridNew)) {
                fkShakeridOld.getResponsesSet().remove(responses);
                fkShakeridOld = em.merge(fkShakeridOld);
            }
            if (fkShakeridNew != null && !fkShakeridNew.equals(fkShakeridOld)) {
                fkShakeridNew.getResponsesSet().add(responses);
                fkShakeridNew = em.merge(fkShakeridNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = responses.getIdentity();
                if (findResponses(id) == null) {
                    throw new NonexistentEntityException("The responses with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Responses responses;
            try {
                responses = em.getReference(Responses.class, id);
//                responses.getIdentity();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The responses with id " + id + " no longer exists.", enfe);
            }
//            Shakers fkShakerid = responses.getFkShakerid();
//            if (fkShakerid != null) {
//                fkShakerid.getResponsesSet().remove(responses);
//                fkShakerid = em.merge(fkShakerid);
//            }
            em.remove(responses);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Responses> findResponsesEntities() {
        return findResponsesEntities(true, -1, -1);
    }

    public List<Responses> findResponsesEntities(int maxResults, int firstResult) {
        return findResponsesEntities(false, maxResults, firstResult);
    }

    private List<Responses> findResponsesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Responses.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Responses findResponses(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Responses.class, id);
        } finally {
            em.close();
        }
    }

    public int getResponsesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Responses> rt = cq.from(Responses.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Responses> findResponsesForShakerID(Shakers key) {
        EntityManager em = getEntityManager();
        
        try {
            Query qry = em.createNamedQuery("Responses.findAllResponsesForShaker");
            qry.setParameter("shaker", key);
            List <Responses> resLst = qry.getResultList();
            return resLst;
        } finally {
          em.close();
        }
    }
    
}
