/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher.data.controllers;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import xypher.data.controllers.exceptions.IllegalOrphanException;
import xypher.data.controllers.exceptions.NonexistentEntityException;
import xypher.data.controllers.exceptions.PreexistingEntityException;
import xypher.data.controllers.models.Shakers;
import xypher.data.controllers.models.Users;

/**
 *
 * @author bilorge
 */
public class UsersJpaController implements Serializable {

    public UsersJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Users users) throws PreexistingEntityException, Exception {
        if (users.getShakersSet() == null) {
            users.setShakersSet(new HashSet<Shakers>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Set<Shakers> attachedShakersSet = new HashSet<Shakers>();
            for (Shakers shakersSetShakersToAttach : users.getShakersSet()) {
                shakersSetShakersToAttach = em.getReference(shakersSetShakersToAttach.getClass(), shakersSetShakersToAttach.getIdentity());
                attachedShakersSet.add(shakersSetShakersToAttach);
            }
            users.setShakersSet(attachedShakersSet);
            em.persist(users);
            for (Shakers shakersSetShakers : users.getShakersSet()) {
                Users oldFkUseridOfShakersSetShakers = shakersSetShakers.getFkUserid();
                shakersSetShakers.setFkUserid(users);
                shakersSetShakers = em.merge(shakersSetShakers);
                if (oldFkUseridOfShakersSetShakers != null) {
                    oldFkUseridOfShakersSetShakers.getShakersSet().remove(shakersSetShakers);
                    oldFkUseridOfShakersSetShakers = em.merge(oldFkUseridOfShakersSetShakers);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsers(users.getIduser()) != null) {
                throw new PreexistingEntityException("Users " + users + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Users users) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users persistentUsers = em.find(Users.class, users.getIduser());
            Set<Shakers> shakersSetOld = persistentUsers.getShakersSet();
            Set<Shakers> shakersSetNew = users.getShakersSet();
            List<String> illegalOrphanMessages = null;
            for (Shakers shakersSetOldShakers : shakersSetOld) {
                if (!shakersSetNew.contains(shakersSetOldShakers)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Shakers " + shakersSetOldShakers + " since its fkUserid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Set<Shakers> attachedShakersSetNew = new HashSet<Shakers>();
            for (Shakers shakersSetNewShakersToAttach : shakersSetNew) {
                shakersSetNewShakersToAttach = em.getReference(shakersSetNewShakersToAttach.getClass(), shakersSetNewShakersToAttach.getIdentity());
                attachedShakersSetNew.add(shakersSetNewShakersToAttach);
            }
            shakersSetNew = attachedShakersSetNew;
            users.setShakersSet(shakersSetNew);
            users = em.merge(users);
            for (Shakers shakersSetNewShakers : shakersSetNew) {
                if (!shakersSetOld.contains(shakersSetNewShakers)) {
                    Users oldFkUseridOfShakersSetNewShakers = shakersSetNewShakers.getFkUserid();
                    shakersSetNewShakers.setFkUserid(users);
                    shakersSetNewShakers = em.merge(shakersSetNewShakers);
                    if (oldFkUseridOfShakersSetNewShakers != null && !oldFkUseridOfShakersSetNewShakers.equals(users)) {
                        oldFkUseridOfShakersSetNewShakers.getShakersSet().remove(shakersSetNewShakers);
                        oldFkUseridOfShakersSetNewShakers = em.merge(oldFkUseridOfShakersSetNewShakers);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = users.getIduser();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getIduser();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Set<Shakers> shakersSetOrphanCheck = users.getShakersSet();
            for (Shakers shakersSetOrphanCheckShakers : shakersSetOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Shakers " + shakersSetOrphanCheckShakers + " in its shakersSet field has a non-nullable fkUserid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(users);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Users.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Users findUsers(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Users.class, id);
        } finally {
            em.close();
        }
    }
    
    public Users findByEmail(String email) {
        EntityManager em = getEntityManager();
        try {
            Query qry = em.createNamedQuery("Users.findByEmail");
            qry.setParameter("email", email);
            return (Users) qry.getSingleResult();
        } finally {
            em.close();
        }
    }
    
    public Users findByUsername(String userName) {
        EntityManager em = getEntityManager();
        try {
            Query qry = em.createNamedQuery("Users.findByUsername");
            qry.setParameter("username", userName);
            return (Users) qry.getSingleResult();
        } finally {
            em.close();
        }
    }

    public int getUsersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
