/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher.data.controllers.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author bilorge
 */
@Entity
@Table(name = "variants", catalog = "kryptoKeepr", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"variantId"})})
@NamedQueries({
    @NamedQuery(name = "Variants.findAll", query = "SELECT v FROM Variants v"),
    @NamedQuery(name = "Variants.findByVariantId", query = "SELECT v FROM Variants v WHERE v.variantId = :variantId"),
    @NamedQuery(name = "Variants.findByValue", query = "SELECT v FROM Variants v WHERE v.value = :value"),
    @NamedQuery(name = "Variants.findByCreatedate", query = "SELECT v FROM Variants v WHERE v.createdate = :createdate"),
    @NamedQuery(name = "Variants.findByStatus", query = "SELECT v FROM Variants v WHERE v.status = :status"),
    @NamedQuery(name = "Variants.findByShakerID", query = "SELECT v FROM Variants v WHERE v.fkShakerid = :shakerid"),
    @NamedQuery(name = "Variants.findActiveVariantsForShaker", query = "SELECT v FROM Variants v where v.status = 1 and v.fkShakerid = :shakerid")})
public class Variants implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(nullable = false, length = 80)
    private String variantId;
    @Basic(optional = false)
    @Column(nullable = false, length = 128)
    private String value;
    @Basic(optional = false)
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdate;
    @Basic(optional = false)
    @Column(nullable = false)
    private int status;
    @JoinColumn(name = "fk_shakerid", referencedColumnName = "identity", nullable = false)
    @ManyToOne(optional = false)
    private Shakers fkShakerid;

    public Variants() {
    }

    public Variants(String variantId) {
        this.variantId = variantId;
    }

    public Variants(String variantId, String value, Date createdate, int status) {
        this.variantId = variantId;
        this.value = value;
        this.createdate = createdate;
        this.status = status;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Shakers getFkShakerid() {
        return fkShakerid;
    }

    public void setFkShakerid(Shakers fkShakerid) {
        this.fkShakerid = fkShakerid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (variantId != null ? variantId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Variants)) {
            return false;
        }
        Variants other = (Variants) object;
        if ((this.variantId == null && other.variantId != null) || (this.variantId != null && !this.variantId.equals(other.variantId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ddt.kryptokeypr.db.Variants[ variantId=" + variantId + " ]";
    }

}
