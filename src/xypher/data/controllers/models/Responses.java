/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher.data.controllers.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author bilorge
 */
@Entity
@Table(name = "responses", catalog = "kryptoKeepr", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"IDENTITY"})})
@NamedQueries({
    @NamedQuery(name = "Responses.findAll", query = "SELECT r FROM Responses r"),
    @NamedQuery(name = "Responses.findByIdentity", query = "SELECT r FROM Responses r WHERE r.identity = :identity"),
@NamedQuery(name = "Responses.findAllResponsesForShaker", query = "SELECT r FROM Responses r WHERE r.fkShakerid = :shaker")})
public class Responses implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(nullable = false, length = 80)
    private String identity;
    @Basic(optional = false)
    @Lob
    @Column(nullable = false)
    private byte[] parameters;
    @Basic(optional = false)
    @Lob
    @Column(nullable = false)
    private byte[] response;
    @JoinColumn(name = "fk_shakerid", referencedColumnName = "identity", nullable = false)
    @ManyToOne(optional = false)
    private Shakers fkShakerid;

    public Responses() {
    }

    public Responses(String identity) {
        this.identity = identity;
    }

    public Responses(String identity, byte[] parameters, byte[] response) {
        this.identity = identity;
        this.parameters = parameters;
        this.response = response;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public byte[] getParameters() {
        return parameters;
    }

    public void setParameters(byte[] parameters) {
        this.parameters = parameters;
    }

    public byte[] getResponse() {
        return response;
    }

    public void setResponse(byte[] response) {
        this.response = response;
    }

    public Shakers getFkShakerid() {
        return fkShakerid;
    }

    public void setFkShakerid(Shakers fkShakerid) {
        this.fkShakerid = fkShakerid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (identity != null ? identity.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Responses)) {
            return false;
        }
        Responses other = (Responses) object;
        if ((this.identity == null && other.identity != null) || (this.identity != null && !this.identity.equals(other.identity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ddt.kryptokeypr.db.Responses[ identity=" + identity + " ]";
    }
    
}
