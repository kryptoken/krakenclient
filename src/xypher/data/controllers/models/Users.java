/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher.data.controllers.models;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author bilorge
 */
@Entity
@Table(name = "users", catalog = "kryptoKeepr", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"iduser"}),
//    @UniqueConstraint(columnNames = {"password"}),
    @UniqueConstraint(columnNames = {"email"}),
    @UniqueConstraint(columnNames = {"username"})})
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findByIduser", query = "SELECT u FROM Users u WHERE u.iduser = :iduser"),
    @NamedQuery(name = "Users.findByUsername", query = "SELECT u FROM Users u WHERE u.username = :username"),
    @NamedQuery(name = "Users.findByEmail", query = "SELECT u FROM Users u WHERE u.email = :email"),
    @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password")})
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(nullable = false, length = 80)
    private String iduser;
    @Basic(optional = false)
    @Column(nullable = false, length = 80)
    private String username;
    @Basic(optional = false)
    @Column(nullable = false, length = 256)
    private String email;
    @Basic(optional = false)
    @Column(nullable = false, length = 128)
    private String password;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkUserid")
    private Set<Shakers> shakersSet;

    public Users() {
    }

    public Users(String iduser) {
        this.iduser = iduser;
    }

    public Users(String iduser, String username, String email, String password) {
        this.iduser = iduser;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Shakers> getShakersSet() {
        return shakersSet;
    }

    public void setShakersSet(Set<Shakers> shakersSet) {
        this.shakersSet = shakersSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iduser != null ? iduser.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.iduser == null && other.iduser != null) || (this.iduser != null && !this.iduser.equals(other.iduser))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ddt.kryptokeypr.db.Users[ iduser=" + iduser + " ]";
    }
    
}
