/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher.data.controllers.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author bilorge
 */
@Entity
@Table(name = "tags", catalog = "kryptoKeepr", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"tagid"})})
@NamedQueries({
    @NamedQuery(name = "Tags.findAll", query = "SELECT t FROM Tags t"),
    @NamedQuery(name = "Tags.findByTagid", query = "SELECT t FROM Tags t WHERE t.tagid = :tagid"),
    @NamedQuery(name = "Tags.findByForeignKey", query = "SELECT t FROM Tags t WHERE t.fkShakerid = :shakerid"),
    @NamedQuery(name = "Tags.findByCreatedate", query = "SELECT t FROM Tags t WHERE t.createdate = :createdate")})
public class Tags implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(nullable = false, length = 80)
    private String tagid;
    @Basic(optional = false)
    @Lob
    @Column(nullable = false, length = 16777215)
    private String tags;
    @Basic(optional = false)
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdate;
    @JoinColumn(name = "fk_shakerid", referencedColumnName = "identity", nullable = false)
    @OneToOne(optional = false)
    private Shakers fkShakerid;

    public Tags() {
    }

    public Tags(String tagid) {
        this.tagid = tagid;
    }

    public Tags(String tagid, String tags, Date createdate) {
        this.tagid = tagid;
        this.tags = tags;
        this.createdate = createdate;
    }

    public String getTagid() {
        return tagid;
    }

    public void setTagid(String tagid) {
        this.tagid = tagid;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Shakers getFkShakerid() {
        return fkShakerid;
    }

    public void setFkShakerid(Shakers fkShakerid) {
        this.fkShakerid = fkShakerid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tagid != null ? tagid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tags)) {
            return false;
        }
        Tags other = (Tags) object;
        if ((this.tagid == null && other.tagid != null) || (this.tagid != null && !this.tagid.equals(other.tagid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ddt.kryptokeypr.db.Tags[ tagid=" + tagid + " ]";
    }
    
}
