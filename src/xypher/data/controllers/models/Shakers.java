/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher.data.controllers.models;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author bilorge
 */
@Entity
@Table(name = "shakers", catalog = "kryptoKeepr", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"identity"}),
    @UniqueConstraint(columnNames = {"shaker"})})
@NamedQueries({
    @NamedQuery(name = "Shakers.findAll", query = "SELECT s FROM Shakers s"),
    @NamedQuery(name = "Shakers.findFKUserKey", query = "SELECT s FROM Shakers s WHERE s.fkUserid = :fkey order by s.shaker"),
    @NamedQuery(name = "Shakers.findByIdentity", query = "SELECT s FROM Shakers s WHERE s.identity = :identity"),
    @NamedQuery(name = "Shakers.findByCreatedate", query = "SELECT s FROM Shakers s WHERE s.createdate = :createdate"),
    @NamedQuery(name = "Shakers.findByPattern", query = "SELECT s FROM Shakers s WHERE s.pattern = :pattern"),
    @NamedQuery(name = "Shakers.findByShaker", query = "SELECT s FROM Shakers s WHERE s.shaker = :shaker"),
    @NamedQuery(name = "Shakers.findByUsername", query = "SELECT s FROM Shakers s WHERE s.username = :username"),
    @NamedQuery(name = "Shakers.findByVersion", query = "SELECT s FROM Shakers s WHERE s.version = :version")})
public class Shakers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(nullable = false, length = 80)
    private String identity;
    @Basic(optional = false)
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdate;
    @Basic(optional = false)
    @Column(nullable = false, length = 128)
    private String pattern;
    @Basic(optional = false)
    @Column(nullable = false, length = 50)
    private String shaker;
    @Basic(optional = false)
    @Column(nullable = false, length = 45)
    private String username;
    @Basic(optional = false)
    @Column(nullable = false)
    private int version;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkShakerid")
    private Set<Responses> responsesSet;
    @JoinColumn(name = "fk_userid", referencedColumnName = "iduser", nullable = false)
    @ManyToOne(optional = false)
    private Users fkUserid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkShakerid")
    private Set<Variants> variantsSet;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "fkShakerid")
    private Set<Tags> tags;

    public Shakers() {
    }

    public Shakers(String identity) {
        this.identity = identity;
    }

    public Shakers(String identity, Date createdate, String pattern, String shaker, String username, int version) {
        this.identity = identity;
        this.createdate = createdate;
        this.pattern = pattern;
        this.shaker = shaker;
        this.username = username;
        this.version = version;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getShaker() {
        return shaker;
    }

    public void setShaker(String shaker) {
        this.shaker = shaker;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Set<Responses> getResponsesSet() {
        return responsesSet;
    }

    public void setResponsesSet(Set<Responses> responsesSet) {
        this.responsesSet = responsesSet;
    }

    public Users getFkUserid() {
        return fkUserid;
    }

    public void setFkUserid(Users fkUserid) {
        this.fkUserid = fkUserid;
    }

    public Set<Variants> getVariantsSet() {
        return variantsSet;
    }

    public void setVariantsSet(Set<Variants> variantsSet) {
        this.variantsSet = variantsSet;
    }

    public Set<Tags> getTags() {
        return tags;
    }

    public void setTags(Set<Tags> tags) {
        this.tags = tags;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (identity != null ? identity.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Shakers)) {
            return false;
        }
        Shakers other = (Shakers) object;
        if ((this.identity == null && other.identity != null) || (this.identity != null && !this.identity.equals(other.identity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ddt.kryptokeypr.db.Shakers[ identity=" + identity + " ]";
    }
    
}
