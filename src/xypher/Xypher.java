/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xypher;

import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import xypher.data.controllers.ShakersJpaController;
import xypher.data.controllers.models.Shakers;

/**
 *
 * @author bilorge
 */
public class Xypher {
    //@todo: Safe to delete

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("XypherPU");
        ShakersJpaController shakerCon = new ShakersJpaController(emf);
        List<Shakers> shakers = shakerCon.findShakersEntities();
        shakers.stream().forEach((shaker) -> {
            System.out.println("Shaker: " + shaker.toString());
        });
    }    
}
